from xml.etree import ElementTree as et  
import os
from natsort import natsorted, ns

#import arcpy  

if __name__ == '__main__':
	path  = os.path.dirname(os.path.realpath(__file__))
	path_original_xml = os.path.join(path, 'original_xml')
	path_modified_xml = os.path.join(path, 'modified_xml')
	fname_xml_list = sorted([fname_xml for fname_xml in os.listdir(path_original_xml) if os.path.isfile(os.path.join(path_original_xml, fname_xml))])
	fname_list = natsorted(fname_list, key=lambda y: y.lower())
	count = 1
	nb_augment_img = 50
	for fname_xml in fname_xml_list:
		tree = et.parse(path_original_xml+"\\"+fname_xml)
		if (count < (71 * nb_augment_img)):
			tree.find('.//name').text = "inria-remote"
		else:
			tree.find('.//name').text = "inria-phone"
		#tree.write(r"path_modified_xml+fname_xml")
		#tree.write(path_modified_xml+"\\"+fname_xml)
		tree.write(path_modified_xml+"\\"+ str(count) + '.xml') #here the png file not the xml
		print("xml modified at " +path_modified_xml+"\\"+fname_xml )
		count = count + 1