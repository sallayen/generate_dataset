# Generate_Dataset : Pipepeline in order to generate a dataset for Fast-RCNN_inception_v2 model from green chromakey(Python version)

This project was created with the help of Jiri Sedlar.
This repository host the back-end module for the project **iReal**.
For a general description of the project, please refer to the 
[iReal repository](https://gitlab.inria.fr/sallayen/ireal)

  

## Descripton of the project

### Mandatory folders

The main script used is generate_dataset.py. It must be near folders named:

* **mask_augmented** Mask png created from the dataset generation

* **rgb** Green chromakey pictures where there is objects. Here it is inria remote and inria phone (70 pictures each)

* **rgba_augmented** Final PNG from the dataset generation with rgb mixed with bg

* **xml** Xml template used for the dataset generation

* **xml_augmented** Where the xml in correspondance of rgba_augmented are created. It must be used with rgba_augmented in order to create a Fast-RCNN_v2_model

### Additional folder and script

* **Dataset_7000** The generation of a dataset with 7000 pictures (3500 for both objects phone and remote)

* **modify_name_in_xml.py** If you want to modify informations inside the xml


### Launch the dataset generation 

```
python generate_dataset.py

```



