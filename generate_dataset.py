import os
import cv2
import numpy as np
import skimage.transform
import skimage.filters
import skimage.exposure
import skimage.util
import scipy
from xml.etree import ElementTree as et  
from PIL import Image
from natsort import natsorted, ns

# contrast and brightness:
def contrast_brightness(rgba, brightness_change_pct=None, contrast_pct=None):
    rgb = rgba[:, :, :3]
    if brightness_change_pct is None:
        bri_value = 0.025 * np.random.randn()
        bri_value = np.clip(bri_value, -0.10, 0.10)
    else:
        bri_value = brightness_change_pct / 100.
    if contrast_pct is None:
        con_value = 2 ** (0.05 * np.random.randn())
    else:
        con_value = contrast_pct / 100.
    rgb = bri_value * 255. + (con_value * (rgb - 127.5)) + 127.5
    rgb = np.clip(rgb, 0, 255)
    rgba[:, :, :3] = rgb
    return rgba


# flip horizontal:
def flip_horizontal(rgba, flip=None):
    if flip is None:
        flip = np.random.rand() >= 0.5
    if flip:
        rgba = np.fliplr(rgba)
    return rgba 

#flip horizontal coordinate xml
def flip_horizontal_xml(tree):
    width = int(tree.find('.//width').text)
    height = int(tree.find('.//height').text)
    xmin = width - int(tree.find('.//xmax').text)
    ymin = int(tree.find('.//ymax').text)
    xmax = width - int(tree.find('.//xmin').text)
    ymax = int(tree.find('.//ymin').text)
    return tree


# flip vertical:
def flip_vertical(rgba, flip=None):
    if flip is None:
        flip = np.random.rand() >= 0.5
    if flip:
        rgba = np.flipud(rgba)
    return rgba 

#flip horizontal coordinate xml
def flip_vertical_xml(tree):
    width = int(tree.find('.//width').text)
    height = int(tree.find('.//height').text)
    xmin = int(tree.find('.//xmin').text)
    ymin = height - int(tree.find('.//ymax').text)
    xmax = int(tree.find('.//xmax').text)
    ymax = height - int(tree.find('.//ymin').text)
    return tree

# noise:
def noise(rgba, sigma=None):
    rgb = rgba[:, :, :3]
    if sigma is None:
        sigma = abs(np.random.randn()) / 10000.
    rgb = 255.*skimage.util.random_noise(image=rgb/255., mode='gaussian', var=sigma)
    rgb = np.clip(rgb, 0, 255)
    rgba[:, :, :3] = rgb
    return rgba


# blur:
def blur(rgba, sigma=None):
    rgb = rgba[:, :, :3]
    if sigma is None:
        #sigma = abs(np.random.randn() / 32.)
        sigma = abs(np.random.randn() / 1.)
    is_colour = len(rgba.shape) == 3
    rgb = skimage.filters.gaussian(rgb, sigma=sigma, multichannel=is_colour)
    rgb = np.clip(rgb, 0, 255)
    rgba[:, :, :3] = rgb
    if np.shape(rgba)[2] == 4:
        rgba[:, :, 3] = skimage.filters.gaussian(rgba[:, :, 3], sigma=sigma)
        rgba[:, :, 3] = np.clip(rgba[:, :, 3], 0, 255)
    return rgba


# translation:
def translation(rgba, x_trans_pct=None, y_trans_pct=None):
    if x_trans_pct is None:
        x_trans_pct = 100./16 * np.random.randn()
        x_trans_pct = np.clip(x_trans_pct, -2., 2.)
    if y_trans_pct is None:
        y_trans_pct = 100./16 * np.random.randn()
        y_trans_pct = np.clip(y_trans_pct, -2., 2.)

    h = len(rgba)
    w = len(rgba[0])
    x_trans = .01 * x_trans_pct * w
    y_trans = .01 * y_trans_pct * h
    x_trans = int(np.round(x_trans))
    y_trans = int(np.round(y_trans))
    rgba = skimage.transform.warp(rgba, skimage.transform.AffineTransform(translation=(-x_trans, -y_trans)))
    return rgba


# rotation:
def rotation(rgba, angle_deg=None):
    if angle_deg is None:
        angle_deg = 360. * float(np.random.rand())
    angle = angle_deg
    rgba = skimage.transform.rotate(rgba, -angle)
    return rgba


# scale:
def scale(rgba, scale_pct=None):

    if scale_pct is None:
        scale_pct = 2 ** (1/4. * np.random.randn())
        #scale_pct = np.clip(scale_pct, 3./4, 4./3)
        scale_pct = np.clip(scale_pct, 0.5, 2.0)
        scale_pct = 100. * scale_pct * 2.

    h = len(rgba)
    w = len(rgba[0])

    p1x = w/2. - 0.01 * scale_pct * w/2.
    p2x = w/2. + 0.01 * scale_pct * w/2.
    p1y = h/2. - 0.01 * scale_pct * h/2.
    p2y = h/2. + 0.01 * scale_pct * h/2.

    p1x = int(np.round(p1x))
    p2x = int(np.round(p2x))
    p1y = int(np.round(p1y))
    p2y = int(np.round(p2y))

    crop_p1x = max(p1x, 0)
    crop_p1y = max(p1y, 0)
    crop_p2x = min(p2x, w)
    crop_p2y = min(p2y, h)

    cropped_rgba = rgba[crop_p1y:crop_p2y, crop_p1x:crop_p2x]

    x_pad_before = -min(0, p1x)
    x_pad_after = max(0, p2x - w)
    y_pad_before = -min(0, p1y)
    y_pad_after = max(0, p2y - h)
    padding = [(y_pad_before, y_pad_after), (x_pad_before, x_pad_after)]
    is_colour = len(rgba.shape) == 3
    if is_colour:
        padding.append((0, 0))  # color images have an extra dimension

    padded_rgba = np.pad(cropped_rgba, padding, 'constant')
    rgba = skimage.transform.resize(padded_rgba, (h, w))
    return rgba


def augment(rgba):  # TODO: select

    rgba = contrast_brightness(rgba)
    rgba = flip_vertical(rgba)
    rgba = flip_horizontal(rgba)
    rgba = scale(rgba)
    rgba = rotation(rgba)
    rgba = noise(rgba)
    rgba = blur(rgba)
    rgba = translation(rgba)
    return rgba


def paste_rgba_on_background(rgba, bg):
    rgb = rgba[:, :, :3]
    mask = rgba[:, :, 3 ] / 255. ###### test here for the correct paste #########
    mask = np.dstack((mask, mask, mask))
    if len(bg.shape) == 2:
        bg = np.dstack((bg, bg, bg))
    h = rgb.shape[0]
    w = rgb.shape[1]
    h_bg = bg.shape[0]
    w_bg = bg.shape[1]
    s = max(1. * w / w_bg, 1. * h / h_bg)
    bg = skimage.transform.resize(image=bg, output_shape=(np.ceil(s * h_bg), np.ceil(s * w_bg)), order=1, preserve_range=True)
    bg = bg[:h, :w, :]
    rgb_bg = mask * rgb + (1. - mask) * bg
    mask = np.round(mask[:, :, 0])  # save mask values as {0,1} instead of {0,255}
    mask_output = 255. * mask
    #return rgb_bg, mask
    return rgb_bg, mask_output

def paste_png_to_bg(fname_rgba, fname_bg, fname_rgba_bg):
    img = Image.open(fname_rgba, 'r')
    bg = Image.open(fname_bg, 'r')
    text_img = Image.new('RGBA', (896,504), (0, 0, 0, 0))
    text_img.paste(bg, (0,0))
    text_img.paste(img, (0,0), mask=img)
    #center image
    #text_img.paste(bg, ((text_img.width - bg.width) // 2, (text_img.height - bg.height) // 2))
    #text_img.paste(ironman, ((text_img.width - ironman.width) // 2, (text_img.height - ironman.height) // 2), mask=ironman)
    text_img.save(fname_rgba_bg, format="png")
    print(fname_rgba_bg)

######################## rgba_from_green_background ###########################
def rgba_from_rgb_green_bg(fill_holes, fname_rgb, r_minus_g_threshold=0, b_minus_g_threshold=-24):
    rgb = cv2.imread(fname_rgb, 1)
    rgb = np.asarray(rgb, float)
    r = rgb[:,:,2]
    g = rgb[:,:,1]
    b = rgb[:,:,0]

    mask = np.zeros_like(g)  # 1 .. foreground, 0 .. green background
    mask[r - g > r_minus_g_threshold] = 1.
    mask[b - g > b_minus_g_threshold] = 1.  

    kernel_ci = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(3,3))
    kernel_sq = cv2.getStructuringElement(cv2.MORPH_RECT,(3,3))
    mask = cv2.dilate(mask, kernel_ci, iterations = 1)
    mask = cv2.dilate(mask, kernel_sq, iterations = 1)
    mask = cv2.erode(mask, kernel_ci, iterations = 1)

    if (fill_holes):
        mask_foreground = scipy.ndimage.binary_fill_holes(mask).astype(int)
        mask_foreground = 255. * mask_foreground
    else:
        mask_foreground = 255. * mask

    rgba = np.zeros([np.shape(rgb)[0], np.shape(rgb)[1], 4])
    rgba[:,:,2] = r
    rgba[:,:,1] = np.min([g, (r+b)/2], axis=0)  # green
    rgba[:,:,0] = b
    rgba[:,:,3] = mask_foreground  # alpha
    
    return rgba, mask_foreground

def get_bbox_mask(img):
    rows = np.any(img, axis=1)
    cols = np.any(img, axis=0)
    rmin, rmax = np.where(rows)[0][[0, -1]]
    cmin, cmax = np.where(cols)[0][[0, -1]]
    return cmin, cmax, rmin, rmax

def load_and_parse_xml(path_template_xml):
    tree = et.parse(path_template_xml)
    return tree

def save_xml(count, mask_augmented_bg, name_object): 
    xmin, xmax, ymin, ymax = get_bbox_mask(mask_augmented_bg)   
    h = mask_augmented_bg.shape[0]
    w = mask_augmented_bg.shape[1]
    tree.find('.//filename').text = str(count) + '.png'
    tree.find('.//path').text = path_rgba_augmented + '\\' + str(count) + '.png'
    tree.find('.//name').text = name_object
    tree.find('.//width').text = str(w)
    tree.find('.//height').text = str(h)
    tree.find('.//xmin').text = str(xmin)
    tree.find('.//ymin').text = str(ymin)
    tree.find('.//xmax').text = str(xmax)
    tree.find('.//ymax').text = str(ymax)
    tree.write(path_xml_augmented+"\\"+ str(count) + '.xml')



path  = os.path.dirname(os.path.realpath(__file__))
print("path: " + path)
path_rgb = os.path.join(path, 'rgb') 
path_bg = os.path.join(path, 'bg')   
path_rgba_augmented = os.path.join(path, 'rgba_augmented')
path_mask_augmented = os.path.join(path, 'mask_augmented')
path_xml = os.path.join(path, 'xml')
path_xml_augmented = os.path.join(path, 'xml_augmented')
path_template_xml = os.path.join(path_xml,'template_annotation.xml')

fname_list = [fname for fname in os.listdir(path_rgb) if os.path.isfile(os.path.join(path_rgb, fname))]
fname_list = natsorted(fname_list, key=lambda y: y.lower())
fname_bg_list = [fname_bg for fname_bg in os.listdir(path_bg) if os.path.isfile(os.path.join(path_bg, fname_bg))]
fname_bg_list = natsorted(fname_bg_list, key=lambda y: y.lower())
tree = load_and_parse_xml(path_template_xml)

np.random.seed(0)  # set random seed
count = 1;
nb_augment_img = 50
for fname in fname_list:
    fname_rgb = os.path.join(path_rgb, fname)  
    print(fname)
    if (count < (71 * nb_augment_img)):
        rgba, mask = rgba_from_rgb_green_bg(True, fname_rgb, 0 , -24)    
    else:
        rgba, mask = rgba_from_rgb_green_bg(False, fname_rgb, 0 , -24)
    #for idx, fname_bg in enumerate(fname_bg_list):
    for i in range(0, nb_augment_img):
        #fname_bg = os.path.join(path_bg, fname_bg)
        fname_bg = os.path.join(path_bg, str(count) +'.jpg')
        fname_rgba_augmented = os.path.join(path_rgba_augmented, str(count) +'.png')
        fname_mask_augmented_mask = os.path.join(path_mask_augmented, str(count) +'_mask.png')
        rgba_augmented = augment(rgba)
        bg = cv2.imread(fname_bg, 1)
        bg = np.asarray(bg, float)
        rgba_augmented_bg, mask_augmented_bg = paste_rgba_on_background(rgba_augmented, bg)
        cv2.imwrite(fname_rgba_augmented, rgba_augmented_bg)
        cv2.imwrite(fname_mask_augmented_mask, mask_augmented_bg)
        if (count < (71 * nb_augment_img)):
            save_xml(count, mask_augmented_bg, "inria-remote")
        else:
            save_xml(count, mask_augmented_bg, "inria-phone")
        #paste_png_to_bg(fname_rgba_augmented, fname_bg, fname_rgba_augmented)
        print (fname_rgba_augmented)  
        count = count + 1;      
